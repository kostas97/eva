import argparse
from itertools import product

def printGreyCodes(all_codes,d,n, case, case_):
    printList = []

    # Find the path
    for l in range(1,d):
        for k in range(0,n):
            if data[all_codes[l-1]][k] != data[all_codes[l]][k]:
                printList.append(k)
                break
    # Check for cycle
    cycle = []
    difCounter = 0
    difIndex = 0
    for l in range(0,n):
        if data[all_codes[d-1]][l] != data[all_codes[0]][l]:
            difCounter += 1
            difIndex = l
            if difCounter > 1:
            	break
    if difCounter == 1:
        printList.append(difIndex)
        if case == "a" or case =="c":
            print('C', *printList)
        elif case == "b":
            print('B', *printList)
    elif case == "a":
        print('P', *printList)
    if case_ == "f":
        for i in printList:
            print(*data[i], end=" | ")
        print("")

def flip(x, i, beckettQueue, case):

    tempList = [list(k) for k in data_]
    temp = tempList[x]
    if tempList[x][i] == 0:
        temp[i] = 1
        if case == "b":
            beckettQueue.append(i)
    else:
        if case == "a" or case == "c":
            temp[i] = 0
        if case == "b":
            ind = beckettQueue.pop(0)
            temp[ind] = 0;
    return data.index(temp)

def GC_DFS(d, x, max_coord, n, gc, beckettQueue, case, case_):

    if d == pow(2,n):
        for i in gc:
            all_codes.append(i)
        printGreyCodes(all_codes,d,n,case,case_)
        for k in range(0,d):
            all_codes.pop()
        return
    for i in range(0, min(n,max_coord+1)):
        x = flip(x, i, beckettQueue, case)
        if not visited[x]:
            visited[x] = True
            gc.append(x)
            GC_DFS(d+1, x, max(i+1,max_coord), n, gc, beckettQueue, case, case_)
            visited[x] = False
            gc.pop()
        x = flip(x, i, beckettQueue, case)

if __name__ == '__main__':

    # Parse arguments given in cmd
    parser = argparse.ArgumentParser(description='Process arguments.')
    parser.add_argument('-a', required=False, default="",action='store_true')
    parser.add_argument('-b', required=False, default="",action='store_true')
    parser.add_argument('-u', required=False, default="",action='store_true')
    parser.add_argument('-c', required=False, default="",action='store_true')
    parser.add_argument('-p', required=False, default="",action='store_true')
    parser.add_argument('-r', required=False, default="",action='store_true')
    parser.add_argument('-f', required=False, default="",action='store_true')
    parser.add_argument('-m', required=False, default="",action='store_true')
    parser.add_argument('number_of_bits', type=int)

    args = parser.parse_args()

    data_ = list(product([0, 1], repeat = args.number_of_bits))
    data = [list(i) for i in data_]

    gc = []
    gc.append(0)
    beckettQueue = []
    tempQ = []
    all_codes = []
    n = args.number_of_bits
    visited =  [False for i in range(1,pow(2,n)+1)]
    visited[0] = True

    # Implement the asked algorithm declared from the user's cmd arguments
    wrapper = vars(args)
    # d=1, x=0, max_coord=0
    if wrapper['a']:
        if wrapper['f']:
            GC_DFS(1, 0, 0, n, gc, beckettQueue, "a","f")
        else:
            GC_DFS(1, 0, 0, n, gc, beckettQueue, "a","")
    elif wrapper['b']:
        if wrapper['f']:
            GC_DFS(1, 0, 0, n, gc, beckettQueue, "b","f")
        else:
            GC_DFS(1, 0, 0, n, gc, beckettQueue, "b","")
    elif wrapper['c']:
        if wrapper['f']:
            GC_DFS(1, 0, 0, n, gc, beckettQueue, "c","f")
        else:
            GC_DFS(1, 0, 0, n, gc, beckettQueue, "c","")
    else:
    	print("default command line argument is a ['-a']")
    	GC_DFS(1, 0, 0, n, gc, beckettQueue, "a","f")