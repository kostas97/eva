import argparse
from itertools import combinations,chain

def colinear(x,y,x1,y1,x2,y2):
    # Colinear points condition: x-x1 / x2-x1 == y-y1 / y2-y1
    varx1 = float(x-x1)
    varx2 = float(x2-x1)
    if (varx2 == 0 and varx1 != 0):
        return 0
    elif (varx2 == 0 and varx1 == 0):   # linear
        return 2

    vary1 = float(y-y1)
    vary2 = float(y2-y1)
    if (vary2 == 0 and vary1 != 0):
        return 0
    elif (vary2 == 0 and vary1 == 0):   # linear
        return 2

    var1 = float(varx1/varx2)
    var2 = float(vary1/vary2)
    if (var1 == var2):
        return 1
    else:
        return 0

def calculateS(data, N, par):
    # Find every line resulting from 2 points -> s1
    s1=[]
    sub=[]
    for sub in combinations(data, 2):
        s1+=[sub]
    lineNum = len(s1)

    # Check for the rest of the points if they are colinear with the already known lines and add them if they are
    for i in range (0,lineNum):
        for j in range (0,N):
            x = data[j][0]
            y = data[j][1]
            x1 = s1[i][0][0]
            x2 = s1[i][1][0]
            y1 = s1[i][0][1]
            y2 = s1[i][1][1]
            if par == 0:
                if (([x,y] not in s1[i]) and colinear(x,y,x1,y1,x2,y2) != 0):
                    s1[i] += ([x,y],)
            else:
                if (([x,y] not in s1[i]) and colinear(x,y,x1,y1,x2,y2) == 2):
                    s1[i] += ([x,y],)                

    # Delete duplicates (lists with the same values will be kept just once)
    s = []
    temp = set()
    for i in range(0,len(s1)):
        line = s1[i]
        temp = sorted(line)
        if temp not in s:
            s.append(temp)

    # In case of parallel lines
    if par == 1:
        s2 = []
        # delete the rest
        for i in range(0,len(s)):
            if ((s[i][0][0] == s[i][1][0]) or (s[i][0][1] == s[i][1][1])):
                s2.append(s[i])
        # if a point doesn't create a parallel with any other point, then add it as aligned with the x-axe
        for i in range(0,len(data)):
            single = 1
            for j in range(0,len(s2)):
                if data[i] in s2[j]:
                    single = 0
                    break
            if single == 1:
                aligned = [data[i], [data[i][0],0]]
                s2.append(aligned)
        return s2

    return s

def nonGreedyImpl(s, data):
    # Find minimum lines to cover every point
    opt = []
    # For each line combination
    for combination in chain.from_iterable(combinations(s, r) for r in range(len(s)+1)):
        temp = []
        stLines = []
        for line in combination:
            for point in line:
                if point not in temp:
                    temp.append(point)
            stLines.append(line)
        for i in data:
            if i not in temp:
                temp=[]
                break
        if (((len(stLines) < len(opt)) or (len(opt) == 0)) and len(temp)!=0):
            opt = stLines
    return opt

def findMaxUncoveredSublist(s, uncovered):
    # Find line's index with maximum uncovered points
    maxN = 0
    maxInd = []
    for lines in s:
        n = 0
        for points in lines:
            if points in uncovered:
                n = n+1
        if n>maxN:
            maxN = n
            maxInd = s.index(lines)
    return maxInd

def greedyImpl(s, data):
    # Find lines to cover every point
    opt = []
    uncovered = data
    while(uncovered):
        x = findMaxUncoveredSublist(s, uncovered)

        cont = 1
        while(cont):
            cont = 0
            for i in data:
                if i in s[x]:
                    uncovered.remove(i)
                    cont = 1
        opt.append(s[x])
    return opt

def notGreedy(data, N, par):
    # Calculate set of lines (S)
    s = calculateS(data, N, par)
    # Find and return optimal solution
    return nonGreedyImpl(s, data)

def greedy(data, N, par):
    # Calculate set of lines (S)
    s = calculateS(data, N, par)
    # Find and return optimal solution
    return greedyImpl(s, data)

def printOptimal(opt):
    # print result
    for lines in opt:
        print(*lines)

if __name__ == '__main__':

    # Parse arguments given in cmd
    parser = argparse.ArgumentParser(description='Process arguments.')
    parser.add_argument('-f', required=False, default="",action='store_true')
    parser.add_argument('-g', required=False, default="",action='store_true')
    parser.add_argument('filename', type=str)
    args = parser.parse_args()

    # Parse points from text file to list of lists struct data
    with open(args.filename) as f:
        cols, rows = list(map(int, f.readline().split()))
        x = [cols,rows]
        data = []
        data.append(x)
        flag = 1
        while flag == 1:
            line = f.readline()
            if line == '':
                flag = 0
            else:
                data.append(list(map(int, line.split()[:2])))

    # Implement the asked algorithm declared from the user's cmd arguments
    wrapper = vars(args)
    N = len(data);

    if wrapper['f']:    #not Greedy
        if wrapper['g']:    #parallel
            printOptimal(notGreedy(data, N, 1))
        else:   #not parallel
            printOptimal(notGreedy(data, N, 0))
    else:   #Greedy
        if wrapper['g']:    #parallel
            printOptimal(greedy(data, N, 1))
        else:   #not parallel
            printOptimal(greedy(data, N, 0))
